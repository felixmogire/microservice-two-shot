from django.db import models


class LocationVO(models.Model):
    import_id = models.IntegerField(unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.IntegerField(null=True)
    shelf_number = models.IntegerField(null=True)

    def __str__(self):
        return f"{self.closet_name}"

class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(blank=True)
    location = models.ForeignKey(
        LocationVO,
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )

    def __str__(self):
        return f"{self.style_name}"

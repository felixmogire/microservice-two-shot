import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState, useEffect} from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import ShoeList from './Shoelist';
import ShoeForm from './Shoeform';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats/">
            <Route path='' element={<HatsList />}/>
            <Route path='new/' element={<HatsForm/>} />
          </Route>
          <Route path="shoes">
            <Route path="" element={<ShoeList shoes={props.shoes} />} />
            <Route path="new" element={<ShoeForm />} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
